provider "aws" {
  region = var.region
}

resource "aws_vpc" "msk_vpc" {
  cidr_block = var.vpc_cidr
}

resource "aws_subnet" "msk_subnets" {
  count             = length(var.subnet_cidrs)
  vpc_id            = aws_vpc.msk_vpc.id
  cidr_block        = var.subnet_cidrs[count.index]
  availability_zone = element(var.availability_zones, count.index)
}

resource "aws_security_group" "msk_sg" {
  vpc_id = aws_vpc.msk_vpc.id

  ingress {
    from_port   = 9092
    to_port     = 9092
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_msk_cluster" "msk_cluster" {
  cluster_name           = var.cluster_name
  kafka_version          = var.kafka_version
  number_of_broker_nodes = var.number_of_broker_nodes

  broker_node_group_info {
    instance_type   = var.broker_instance_type
    client_subnets  = aws_subnet.msk_subnets[*].id
    security_groups = [aws_security_group.msk_sg.id]
  }

  encryption_info {
    encryption_at_rest_kms_key_arn = aws_kms_key.msk_key.arn
  }

  configuration_info {
    arn      = var.configuration_arn
    revision = var.configuration_revision
  }

  logging_info {
    broker_logs {
      cloudwatch_logs {
        enabled         = true
        log_group       = aws_cloudwatch_log_group.msk_log_group.name
      }
    }
  }
}

resource "aws_cloudwatch_log_group" "msk_log_group" {
  name              = "/aws/msk/${var.cluster_name}"
  retention_in_days = 7
}

resource "aws_kms_key" "msk_key" {
  description = "KMS key for MSK cluster encryption"
}

resource "aws_iam_role" "msk_iam_role" {
  name = "msk-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "kafka.amazonaws.com"
        }
      }
    ]
  })
}

output "msk_cluster_arn" {
  value = aws_msk_cluster.msk_cluster.arn
}

output "msk_security_group_id" {
  value = aws_security_group.msk_sg.id
}
