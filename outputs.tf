output "msk_cluster_arn" {
  description = "ARN of the MSK cluster"
  value       = aws_msk_cluster.msk_cluster.arn
}

output "msk_security_group_id" {
  description = "Security group ID for the MSK cluster"
  value       = aws_security_group.msk_sg.id
}
