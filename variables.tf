variable "region" {
  description = "AWS region"
  type        = string
}

variable "vpc_cidr" {
  description = "CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnet_cidrs" {
  description = "List of CIDR blocks for the subnets"
  type        = list(string)
}

variable "availability_zones" {
  description = "List of availability zones"
  type        = list(string)
}

variable "cluster_name" {
  description = "Name of the MSK cluster"
  type        = string
}

variable "kafka_version" {
  description = "Kafka version for the MSK cluster"
  type        = string
  default     = "2.6.0"
}

variable "number_of_broker_nodes" {
  description = "Number of broker nodes in the MSK cluster"
  type        = number
  default     = 3
}

variable "broker_instance_type" {
  description = "Instance type for the broker nodes"
  type        = string
  default     = "kafka.m5.large"
}

variable "configuration_arn" {
  description = "ARN of the MSK configuration"
  type        = string
}

variable "configuration_revision" {
  description = "Revision of the MSK configuration"
  type        = number
}
