### Документация для Terraform модуля AWS MSK

#### Описание

Этот Terraform модуль предназначен для развертывания управляемого кластера Apache Kafka с использованием сервиса Amazon Managed Streaming for Apache Kafka (AWS MSK). Модуль включает создание необходимых компонентов, таких как VPC, подсети, группы безопасности и роли IAM.

#### Параметры

- **region** (строка): Регион AWS, где будет развернут кластер MSK.
- **vpc_cidr** (строка): CIDR блок для VPC.
- **subnet_cidrs** (список строк): Список CIDR блоков для подсетей.
- **availability_zones** (список строк): Список зон доступности.
- **cluster_name** (строка): Имя кластера MSK.
- **kafka_version** (строка): Версия Apache Kafka для кластера MSK.
- **number_of_broker_nodes** (число): Количество брокеров в кластере MSK.
- **broker_instance_type** (строка): Тип инстансов для брокеров.
- **configuration_arn** (строка): ARN конфигурации MSK.
- **configuration_revision** (число): Ревизия конфигурации MSK.

#### Выходные значения

- **msk_cluster_arn**: ARN развернутого кластера MSK.
- **msk_security_group_id**: ID группы безопасности для кластера MSK.

#### Пример использования

```hcl
module "msk" {
  source = "./path/to/terraform-aws-msk"

  region                  = "us-west-2"
  vpc_cidr                = "10.0.0.0/16"
  subnet_cidrs            = ["10.0.1.0/24", "10.0.2.0/24"]
  availability_zones      = ["us-west-2a", "us-west-2b"]
  cluster_name            = "my-msk-cluster"
  kafka_version           = "2.6.0"
  number_of_broker_nodes  = 3
  broker_instance_type    = "kafka.m5.large"
  configuration_arn       = "arn:aws:kafka:us-west-2:123456789012:configuration/my-configuration/abcdef123456"
  configuration_revision  = 1
}
```

#### Шаги по развертыванию

1. Убедитесь, что у вас установлен и настроен Terraform.
2. Создайте новый Terraform конфигурационный файл (например, `main.tf`) и добавьте в него вышеуказанный пример использования.
3. Инициализируйте Terraform, выполнив команду `terraform init`.
4. Примените конфигурацию, выполнив команду `terraform apply`.
5. Подтвердите применение изменений, введя `yes`, когда будет предложено.

Этот модуль автоматически создаст VPC, подсети, группу безопасности, кластер MSK, группу логов CloudWatch и ключ KMS для шифрования. Вы можете настроить параметры модуля в соответствии с вашими требованиями.